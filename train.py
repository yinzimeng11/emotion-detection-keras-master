

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

nb_epoch = 20
batch_size = 128

# data generator
data_generator = ImageDataGenerator(
                        featurewise_center=False,
                        featurewise_std_normalization=False,
                        rotation_range=10,
                        width_shift_range=0.1,
                        height_shift_range=0.1,
                        zoom_range=.1,
                        horizontal_flip=True)


es = EarlyStopping(monitor='val_loss', patience = 10, mode = 'min', restore_best_weights=True)

history = model.fit_generator(data_generator.flow(x_train, y_train, batch_size),
                                steps_per_epoch=len(x_train) / batch_size,
                                epochs=nb_epoch,
                                verbose=1,
                                callbacks = [es],
                                validation_data=(x_eval, y_eval))