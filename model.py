from keras import models
from keras import layers
from keras import optimizers
from keras.losses import categorical_crossentropy
from keras.layers.convolutional import MaxPooling2D
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping

model = models.Sequential()

names = ['emotions', 'pixels', 'usage']
usages = ['Training', 'PublicTest', 'PrivateTest']
emotion_label = ['Anger', 'Disgust', 'Fear', 'Happy', 'Sad', 'Surprise', 'Neutral']
input_size = (48, 48, 1)

# modulue 1
model.add(layers.Conv2D(32, (3,3), padding='same', input_shape=input_size))
model.add(layers.normalization.BatchNormalization())
model.add(layers.Activation('relu'))
model.add(layers.Conv2D(32, (3,3), padding='same'))
model.add(layers.normalization.BatchNormalization())
model.add(layers.Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2),strides=(2, 2)))

# modulue 2
model.add(layers.Conv2D(64, (3,3), padding='same'))
model.add(layers.normalization.BatchNormalization())
model.add(layers.Activation('relu'))
model.add(layers.Conv2D(64, (3,3), padding='same'))
model.add(layers.normalization.BatchNormalization())
model.add(layers.Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2),strides=(2, 2)))


# modulue 3
model.add(layers.Conv2D(128, (3,3), padding='same'))
model.add(layers.normalization.BatchNormalization())
model.add(layers.Activation('relu'))
model.add(layers.Conv2D(128, (3,3), padding='same'))
model.add(layers.normalization.BatchNormalization())
model.add(layers.Activation('relu'))
model.add(MaxPooling2D(pool_size=(2,2),strides=(2, 2)))


# adding dense connected neural network

# Dense 1
model.add(layers.Flatten())
model.add(layers.Dense(128))
model.add(layers.normalization.BatchNormalization())
model.add(layers.Activation('relu'))

# Dense 2
model.add(layers.Dense(64))
model.add(layers.normalization.BatchNormalization())
model.add(layers.Activation('relu'))


# Dense 3
model.add(layers.Dense(32))
model.add(layers.normalization.BatchNormalization())
model.add(layers.Activation('relu'))


# output using softmax
model.add(layers.Dense(len(emotion_label), activation='softmax'))
model.summary()