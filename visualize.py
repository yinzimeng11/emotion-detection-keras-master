# demo image
from keras.preprocessing import image

img = image.load_img('./data/demo_sad.jpg', target_size=(48, 48))
img_tensor = image.img_to_array(img)

img_tensor = img_tensor[:48,:48,:1].reshape(1, 48, 48, 1)

# img_tensor = np.expand_dims(img_tensor, axis=0)
img_tensor /= 255.



print(img_tensor.shape)
plt.imshow(img_tensor[0].reshape(48, 48), cmap='gray')

# demo image
from keras.preprocessing import image

# load and convert the img to grey scale
img = image.load_img('./data/demo_happy.jpg', target_size=(48, 48))
img_tensor = image.img_to_array(img)
img_tensor = img_tensor[:48,:48,:1].reshape(1, 48, 48, 1)

# img_tensor = np.expand_dims(img_tensor, axis=0)
img_tensor /= 255.



print(img_tensor.shape)
plt.imshow(img_tensor[0].reshape(48,48), cmap='gray')


layer_output = [layer.output for layer in model.layers[:20]]
activation_model = models.Model(inputs=model.input, outputs=layer_output)
activations = activation_model.predict(img_tensor)

# display the picture and the predicted result
plt.imshow(img_tensor.reshape(48, 48), cmap='gray')
plt.title('sad')

predict_result = np.argmax(model.predict(img_tensor))
print('The model predict the picture to be {}'.format(emotion_label[predict_result]))


chosen_picture_index = 3

layer_output = [layer.output for layer in model.layers[:23]]
activation_model = models.Model(inputs=model.input, outputs=layer_output)
activations = activation_model.predict(x_test[chosen_picture_index].reshape(1, 48, 48, 1))

# display the picture and the predicted result
plt.imshow(x_test[chosen_picture_index].reshape(48, 48), cmap='gray')
plt.title(emotion_label[int(np.argwhere(y_test[chosen_picture_index]))])

predict_result = np.argmax(model.predict(x_test[chosen_picture_index].reshape(1, 48, 48, 1)))
print('The model predict the picture to be {}'.format(emotion_label[predict_result]))


# First Conv2d
first_layer_activation = activations[0]

fig, axes = plt.subplots(3, 8, figsize=(9, 6), subplot_kw={'xticks': [], 'yticks': []})

fig.subplots_adjust(left=0.03, right=0.97, hspace=0.3, wspace=0.05)

# print(axes.flat)

for index in range(len(axes.flat)):
    axes.flat[index].imshow(first_layer_activation[0,:,:, index], cmap='gray')


# third Conv2d
first_layer_activation = activations[7]

fig, axes = plt.subplots(3, 8, figsize=(9, 6), subplot_kw={'xticks': [], 'yticks': []})

fig.subplots_adjust(left=0.03, right=0.97, hspace=0.3, wspace=0.05)

# print(axes.flat)

for index in range(len(axes.flat)):
    axes.flat[index].imshow(first_layer_activation[0,:,:, index], cmap='gray')


